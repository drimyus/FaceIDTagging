# !/usr/bin/python


# Import the OpenCV and dlib libraries
import cv2
import dlib

import threading
import time


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

# configure
SIDE_THRESH = 0.4
SKIPS = 4
MIN_FRAME_CNT = 100

SEND_FACE_SIZE = 150  # the face size for sending
SCALE_FACTOR = 2  # fact or for decreasing the original frame
MARGIN = 10

JAW_POINTS = list(range(0, 17))
FACE_POINTS = list(range(17, 68))
RIGHT_BROW_POINTS = list(range(17, 22))
LEFT_BROW_POINTS = list(range(22, 27))
NOSE_POINTS = list(range(27, 36))
RIGHT_EYE_POINTS = list(range(36, 42))
LEFT_EYE_POINTS = list(range(42, 48))
MOUTH_POINTS = list(range(48, 60))
LIP_POINTS = list(range(60, 68))


def _getLandmarks(im, rect):
    points = []
    for p in predictor(im, rect).parts():
        points.append((p.x, p.y))
    return points
    # return np.matrix([[p.x, p.y] for p in predictor(im, rect).parts()])


def _detectFaces(frame, upsampling):

    rects = detector(frame, upsampling)

    res_rects = []

    # the pose of detected the face
    for rect in rects:

        points = _getLandmarks(frame, rect)

        # only face for front side
        left = abs(points[39][0] - points[0][0])
        right = abs(points[42][0] - points[16][0])
        if right == 0 or left == 0:
            continue
        elif float(left)/right > SIDE_THRESH and float(right)/left > SIDE_THRESH:
            res_rects.append([rect.left(), rect.top(), rect.width(), rect.height()])

    return res_rects


# We are not doing really face recognition
def _doRecognizePerson(faceNames, fid):

    time.sleep(0.5)
    faceNames[fid] = "Person " + str(fid)


# send face to server
def _sendFace(face):

    # Send the detected face to sever
    face = cv2.resize(face, (SEND_FACE_SIZE, SEND_FACE_SIZE))
    # send_face_to_server(face)
    cv2.imshow('send', face)


def doDetectAndTrackMultipleFaces():

    # Open the first webcame device
    cap = cv2.VideoCapture('video.mp4')
    
    # Setting the output video properties
    video_info = {'width': int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                  'height': int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))}
    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)  # jump to 0th frame (init frame)
    outWidth = int(video_info['width'] / SCALE_FACTOR)
    outHeight = int(video_info['height'] / SCALE_FACTOR)

    # Create two opencv named windows
    # cv2.namedWindow("base-image", cv2.WINDOW_AUTOSIZE)
    # cv2.namedWindow("result-image", cv2.WINDOW_AUTOSIZE)

    # Position the windows next to eachother
    # cv2.moveWindow("base-image", 0, 100)
    # cv2.moveWindow("result-image", 400, 100)

    # Start the window thread for the two windows we are using
    # cv2.startWindowThread()

    # The color of the rectangle we draw around the face
    rectangleColor = (255, 255, 0)
    fontColor = (0, 0, 255)

    # variables holding the current frame number and the current faceid
    frameCounter = 0
    currentFaceID = 1

    # Variables holding the correlation trackers and the name per faceid
    faceTrackers = {}
    faceNames = {}

    try:

        while True:

            # Retrieve the latest image from the webcam
            rc, fullSizeBaseImage = cap.read()
            if not rc:
                print 'Error: Camera was disconnect.'
                break

            # Resize the image to 320x240
            baseImage = cv2.resize(fullSizeBaseImage, (outWidth, outHeight))

            # Check if a key was pressed and if it was q, then break
            # if it was 'n', then skip 1000 frames
            # from the infinite loop
            pressedKey = cv2.waitKey(2)
            if pressedKey == ord('q'):
                break
            if pressedKey == ord('n'):
                pos = cap.get(cv2.CAP_PROP_POS_FRAMES)
                cap.set(cv2.CAP_PROP_POS_FRAMES, pos + 1000)
                
            # Result image is the image we will show the user, which is a
            # combination of the original image from the webcam and the
            # overlayed rectangle for the largest face
            resultImage = baseImage.copy()

            # STEPS:
            # * Update all trackers and remove the ones that are not 
            #   relevant anymore
            # * Every 10 frames:
            #       + Use face detection on the current frame and look
            #         for faces. 
            #       + For each found face, check if centerpoint is within
            #         existing tracked box. If so, nothing to do
            #       + If centerpoint is NOT in existing tracked box, then
            #         we add a new tracker with a new face-id

            # Increase the framecounter
            frameCounter += 1 

            # Update all the trackers and remove the ones for which the update
            # indicated the quality was not good enough
            fidsToDelete = []

            for fid in faceTrackers.keys():
                trackingQuality = faceTrackers[fid].update(baseImage)

                # If the tracking quality is good enough, we must delete
                # this tracker
                if trackingQuality < 7:
                    fidsToDelete.append(fid)

            for fid in fidsToDelete:
                print("Removing fid " + str(fid) + " from list of trackers")
                faceTrackers.pop(fid, None)

            # Every 10 frames, we will have to determine which faces
            # are present in the frame
            if (frameCounter % SKIPS) == 0:

                # For the face detection, we need to make use of a gray
                # colored image so we will convert the baseImage to a
                # gray-based image

                gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)
                # Now use the haar cascade detector to find all faces
                # in the image

                faces = _detectFaces(gray, 0)

                # Loop over all faces and check if the area for this
                # face is the largest so far
                # We need to convert it to int here because of the
                # requirement of the dlib tracker. If we omit the cast to
                # int here, you will get cast errors since the detector
                # returns numpy.int32 and the tracker requires an int

                for (_x, _y, _w, _h) in faces:
                    x = int(_x)
                    y = int(_y)
                    w = int(_w)
                    h = int(_h)

                    # calculate the centerpoint
                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h

                    # Variable holding information which faceid we 
                    # matched with
                    matchedFid = None

                    # Now loop over all the trackers and check if the
                    # centerpoint of the face is within the box of a
                    # tracker
                    for fid in faceTrackers.keys():

                        tracked_position = faceTrackers[fid].get_position()
                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())
                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())

                        # calculate the centerpoint
                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        # check if the centerpoint of the face is within the
                        # rectangleof a tracker region. Also, the centerpoint
                        # of the tracker region must be within the region
                        # detected as a face. If both of these conditions hold
                        # we have a match

                        if ((t_x <= x_bar <= (t_x + t_w)) and
                                (t_y <= y_bar <= (t_y + t_h)) and
                                (x <= t_x_bar <= (x + w)) and
                                (y <= t_y_bar <= (y + h))):
                            matchedFid = fid

                    # If no matched fid, then we have to create a new tracker
                    if matchedFid is None:

                        print("Creating new tracker " + str(currentFaceID))

                        # Create and store the tracker
                        tracker = dlib.correlation_tracker()

                        tracker.start_track(baseImage,
                                            dlib.rectangle(x - MARGIN, y - MARGIN, x + w + MARGIN, y + h + MARGIN))

                        faceTrackers[currentFaceID] = tracker

                        # send the detected face to server
                        _sendFace(baseImage[max(y - MARGIN, 0):min(y + h + MARGIN, outHeight)
                                  , max(x - MARGIN, 0):min(x + w + MARGIN, outWidth)])

                        # Start a new thread that is used to simulate
                        # face recognition. This is not yet implemented in this
                        # version :)
                        t = threading.Thread(target=_doRecognizePerson,
                                             args=(faceNames, currentFaceID))

                        t.start()
                        # Increase the currentFaceID counter
                        currentFaceID += 1

            # Now loop over all the trackers we have and draw the rectangle
            # around the detected faces. If we 'know' the name for this person
            # (i.e. the recognition thread is finished), we print the name
            # of the person, otherwise the message indicating we are detecting
            # the name of the person

            for fid in faceTrackers.keys():

                tracked_position = faceTrackers[fid].get_position()
                t_x = int(tracked_position.left())
                t_y = int(tracked_position.top())
                t_w = int(tracked_position.width())
                t_h = int(tracked_position.height())

                cv2.rectangle(resultImage, (t_x, t_y),
                              (t_x + t_w, t_y + t_h),
                              rectangleColor, 2)

                if fid in faceNames.keys():
                    cv2.putText(resultImage, faceNames[fid],
                                (int(t_x), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, fontColor, 1)

                else:
                    cv2.putText(resultImage, "New Person...",
                                (int(t_x), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, fontColor, 1)

            # Since we want to show something larger on the screen than the
            # original 320x240, we resize the image again

            # Note that it would also be possible to keep the large version
            # of the baseimage and make the result image a copy of this large
            # base image and use the scaling factor to draw the rectangle
            # at the right coordinates.

            # Finally, we want to show the images on the screen
            cv2.imshow("base-image", baseImage)
            cv2.imshow("result-image", resultImage)

    # To ensure we can also deal with the user pressing Ctrl-C in the console
    # we have to check for the KeyboardInterrupt exception and break out of
    # the main loop

    except KeyboardInterrupt as e:
        pass

    # Destroy any OpenCV windows and exit the application
    cv2.destroyAllWindows()
    exit(0)


if __name__ == '__main__':

    doDetectAndTrackMultipleFaces()
