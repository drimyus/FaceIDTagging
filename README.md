### Detecting and tracking a face with Python and OpenCV ###

* Detecting a face
* After we decided to make use of Python, the first feature we would need for performing face recognition is to detect where in the current field of vision a face is present.

Anaconda with Python 3.5
OpenCV 3.1.0
dlib 19.1.0

### Tracking the face ###

The above code for face detection has some drawbacks:

* the code might be computationally expensive
* If the detected person is turning his/her head slightly, the haar cascade might not detect the face anymore
* Very difficult to keep track of a face between frames (i.e. to later only do face recognition one a detected face once and not in every loop).

A better approach for this is to do the detection of the face once and then use the correlation tracker from the excellent dlib library to just keep track of the relevant region from frame to frame.

* face age gender detection is combined with face tracking module.

note: for using the caffe's age-gender model, integer(range 0~255) numpy array of face is converted to float32 ndarray(range 0~1.0)
 