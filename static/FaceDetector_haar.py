import numpy as np
import cv2
from matplotlib import pyplot as plt

face_cascade_1 = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
face_cascade_2 = cv2.CascadeClassifier('haarcascade_frontalface_alt2.xml')
face_cascade_3 = cv2.CascadeClassifier('haarcascade_frontalface_alt_tree.xml')
face_cascade_4 = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

cap = cv2.VideoCapture('Walking Next to People - YouTube.mp4')

cnt = 0
while cap.isOpened():

    ret, img = cap.read()
    # img = cv2.resize(img, (480, 320))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    cnt += 1
    if cnt % 1 is not 0:
        continue

    # faces1 = face_cascade_1.detectMultiScale(gray, 1.9, 5)
    # faces2 = face_cascade_2.detectMultiScale(gray, 1.9, 5)
    # faces3 = face_cascade_3.detectMultiScale(gray, 1.9, 5)
    faces4 = face_cascade_1.detectMultiScale(gray, 1.3, 5)

    # for (x, y, w, h) in faces1:
    #     cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    #     roi_gray = gray[y:y + h, x:x + w]
    #     roi_color = img[y:y + h, x:x + w]

    # for (x, y, w, h) in faces2:
    #     cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    #     roi_gray = gray[y:y + h, x:x + w]
    #     roi_color = img[y:y + h, x:x + w]

    # for (x, y, w, h) in faces3:
    #     cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
    #     roi_gray = gray[y:y + h, x:x + w]
    #     roi_color = img[y:y + h, x:x + w]
    #
    for (x, y, w, h) in faces4:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
        roi_gray = gray[y:y + h, x:x + w]
        roi_color = img[y:y + h, x:x + w]

    cv2.imshow("Faces", img)
    cv2.waitKey(1)

    if cv2.waitKey(1) & 0xFF == ord('q'):  # quit when 'q' keyword is pressed
        break

cap.release()
cv2.destroyAllWindows()



