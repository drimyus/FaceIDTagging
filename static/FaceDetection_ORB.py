# !/usr/bin/python

import cv2
import dlib as dlib

import scipy
from skimage.feature import local_binary_pattern

import numpy as np
import datetime
import sys

PREDICTOR_PATH = 'shape_predictor_68_face_landmarks.dat'
SCALE_FACTOR = 2
SKIP_FACTOR = 5
CROP_FACE_SIZE = 150

MIN_FEATURE_DIS_THRESH = 50
MIN_DIS = 3000000.0

INC_FRAME_CNT = 1
MIN_FRAME_CNT = 20

id_tagged_face_list = []     # List for faces appeared on the frame
id_tagged_lbp_list = []      # List for the lbp features of each face on the id_tagged_face_list
id_tagged_interval_cnt = []  # List for the counts of the duration frames of each face on the id_tagged_face_list

id_tagged_num_list = []        # List for the unique id of the detected faces from video
unique_id = 0


FACE_POINTS = list(range(17, 68))
MOUTH_POINTS = list(range(48, 61))
LIP_POINTS = list(range(62, 68))
RIGHT_BROW_POINTS = list(range(17, 22))
LEFT_BROW_POINTS = list(range(22, 27))
RIGHT_EYE_POINTS = list(range(36, 42))
LEFT_EYE_POINTS = list(range(42, 48))
NOSE_POINTS = list(range(27, 35))
JAW_POINTS = list(range(0, 17))


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(PREDICTOR_PATH)
# Initiate the ORB detector
orb = cv2.ORB_create(nfeatures=500)
# orb = cv2.SIFT(nfeatures=500)
# orb = cv2.xfeatures2d.SIFT_create()
# Create BFMatcher object
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)


def detect_faces(frame):

    rects = detector(frame, 0)
    return rects


def get_lbp_features(img, neighbour=8, radius=1, method='default'):

    gray_image = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    lbp_image = local_binary_pattern(gray_image, P=neighbour, R=radius, method=method)
    lbp_hist = scipy.stats.itemfreq(lbp_image)

    histogram = np.zeros(256)
    for i in range(len(lbp_hist)):
        histogram[int(lbp_hist[i][0])] = int(lbp_hist[i][1])

    return histogram


def seek_similar_face(face):

    global id_tagged_face_list
    global id_tagged_lbp_list

    cv2.normalize(face, face, 0, 255, cv2.NORM_MINMAX)

    lbp1 = get_lbp_features(face)

    min_dis = 0.0

    idx = -1

    if len(id_tagged_face_list) == 0:
        return idx, lbp1, face

    for i, lbp2 in enumerate(id_tagged_lbp_list):
        dis = 0.0
        for j in range(0, 256):
            dis += (lbp1[j] - lbp2[j]) ** 2

        # print dis

        if min_dis == 0.0:
            min_dis = dis
            idx = i

        if dis < min_dis:
            min_dis = dis
            idx = i

    # print min_dis
    if MIN_DIS < min_dis:
        idx = -1

    # # Find the keypoints and descriptor with ORB
    # kp1, des1 = orb.detectAndCompute(face, None)
    # kp2, des2 = orb.detectAndCompute(id_tagged_face_list[idx], None)
    #
    # if des1 is None or des2 is None:
    #     idx = -1
    # else:
    #     # Match descriptors
    #     matches = bf.match(des1, des2)
    #     matches = sorted(matches, key=lambda m: m.distance)
    #
    #     if matches[0].distance > MIN_FEATURE_DIS_THRESH:
    #         idx = -1
    #
    #     if len(matches) != 0:
    #         print matches[0].distance, idx

    # face = cv2.drawKeypoints(face, kp1[:10], None, color=(255, 255, 0), flags=cv2.DrawMatchesFlags_DEFAULT)

    return idx, lbp1, face


def send_face_to_sever(face, uni_id):
    # Send the removed face image to server

    # Display the removed face image
    cv2.imshow('removed', face)
    # print uni_id


def display_id_tag_face_list():

    global id_tagged_face_list

    if len(id_tagged_face_list) != 0:

        list_face = np.zeros((CROP_FACE_SIZE, CROP_FACE_SIZE * len(id_tagged_face_list), 3), np.uint8)

        for i, face in enumerate(id_tagged_face_list):
            list_face[:, i * CROP_FACE_SIZE:(i + 1) * CROP_FACE_SIZE] = face

        cv2.imshow('id_tagged_face_list', list_face)


def id_tagging(frame):

    global id_tagged_face_list
    global id_tagged_lbp_list
    global id_tagged_interval_cnt
    global id_tagged_num_list
    global unique_id

    # Detect the faces on frame and returns the rects as a result
    face_rects = detect_faces(frame)
    temp_id_list = []
    new_face_rects = []

    # Add the faces to the list
    if len(id_tagged_face_list) == 0:
        for r in face_rects:

            face = frame[r.top():r.bottom(), r.left():r.right()]
            if r.top() < 0 or r.bottom() > frame.shape[0] or r.left() < 0 or r.right() > frame.shape[1]:
                continue

            new_face_rects.append(r)

            face = cv2.resize(face, (CROP_FACE_SIZE, CROP_FACE_SIZE))
            lbp_feature = get_lbp_features(face)

            id_tagged_face_list.append(face)
            id_tagged_lbp_list.append(lbp_feature)
            id_tagged_interval_cnt.append(INC_FRAME_CNT)

            unique_id += 1
            id_tagged_num_list.append(unique_id)
            temp_id_list.append(unique_id)

    else:
        # Upgrade the count of appeared frames for each face
        for i in range(len(id_tagged_interval_cnt)):
            id_tagged_interval_cnt[i] += INC_FRAME_CNT 
        
        for r in face_rects:

            face = frame[r.top():r.bottom(), r.left():r.right()]

            if r.top() < 0 or r.bottom() > frame.shape[0] or r.left() < 0 or r.right() > frame.shape[1]:
                continue
            new_face_rects.append(r)

            face = cv2.resize(face, (CROP_FACE_SIZE, CROP_FACE_SIZE))

            # Seek the similar face from the id_tagged list
            index, lbp_feature, face = seek_similar_face(face)

            if index == -1:  # There is no similar face then add to the list
                id_tagged_face_list.append(face)
                id_tagged_lbp_list.append(lbp_feature)
                id_tagged_interval_cnt.append(INC_FRAME_CNT)

                unique_id += 1
                id_tagged_num_list.append(unique_id)

                temp_id_list.append(unique_id)

            elif index in range(len(id_tagged_lbp_list)):  # Replace the original face with the modified one
                id_tagged_face_list[index] = face
                id_tagged_lbp_list[index] = lbp_feature
                id_tagged_interval_cnt[index] = INC_FRAME_CNT

                temp_id_list.append(id_tagged_num_list[index])

            else:
                print 'Error: index is out of range on id_tagging_list!'

            pass

    # Remove the disappeared face from the id_tagged_list
    i = 0
    while i in range(len(id_tagged_interval_cnt)):
        if id_tagged_interval_cnt[i] > MIN_FRAME_CNT:

            send_face_to_sever(id_tagged_face_list[i], id_tagged_num_list[i])

            del id_tagged_interval_cnt[i]
            del id_tagged_face_list[i]
            del id_tagged_lbp_list[i]
            del id_tagged_num_list[i]

        i += 1

    # Display the id_tagged_face_list
    display_id_tag_face_list()

    return new_face_rects, temp_id_list


def main():

    cap = cv2.VideoCapture('1 hour facial test file.mp4')   # in case of video feeds
    # cap = cv2.VideoCapture(0)                             # in case of camera feeds

    # Take first frame
    ret, first_frame = cap.read()
    if first_frame is None:
        print ('Error: There is no camera connected on or cannot read the video file!')
        return

    # Setting the output video properties
    video_info = {'fps': 20,  # frame per seconds
                  'width': int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),      # frame width
                  'height': int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)),    # frame height
                  'fourcc': cap.get(cv2.CAP_PROP_FOURCC),               # video codec
                  'num_of_frames': int(cap.get(cv2.CAP_PROP_FRAME_COUNT))}  # number of current frame

    cap.set(cv2.CAP_PROP_POS_FRAMES, 0)  # jump to 0th frame (init frame)

    new_width = int(video_info['width'] / SCALE_FACTOR)
    new_height = int(video_info['height'] / SCALE_FACTOR)

    cnt = 0
    while cap.isOpened():

        # Read the frame one by one
        ret, frame = cap.read()
        if frame is None:
            print 'Error: Camera was disconnect.'
            break

        resized_frame = cv2.resize(frame, (new_width, new_height))

        # Skip the frames for speeding up
        cnt += 1
        if cnt % SKIP_FACTOR != 0:
            continue

        # Detected the faces and ID_TAGGING
        face_rects, ids = id_tagging(resized_frame)

        # Display the result detected faces
        if len(face_rects) != 0:
            for k, r in enumerate(face_rects):
                cv2.rectangle(resized_frame, (r.left(), r.top()), (r.right(), r.bottom()), (255, 255, 0), 2)

                cv2.putText(resized_frame, 'No_%d' % ids[k], (r.left(), r.top()),
                            cv2.FONT_HERSHEY_SIMPLEX,
                            0.7, (255, 255, 0), 2, cv2.LINE_AA)

        # Show the result
        cv2.imshow('Faces', resized_frame)
        if cv2.waitKey(1) & 0xFF == 27:  # ord('q'):  # quit when 'q' keyword is pressed
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':

    main()
